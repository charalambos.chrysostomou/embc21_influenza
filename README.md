# EMBC21_Influenza

This work is part of the Classification of Influenza Hemagglutinin Protein Sequences using Convolutional Neural Networks published in EMBC 21.

Requirments: Tensorflow > 2.3, Horovod > 0.21