import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import Sequential, losses
from tensorflow.keras.callbacks import ModelCheckpoint,ReduceLROnPlateau
from tensorflow.keras.layers import Conv1D, Dense, MaxPool1D, Flatten, BatchNormalization, Dropout, LeakyReLU
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import save_model, load_model
from scipy.interpolate import interp1d
from tensorflow.keras.utils import plot_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import matthews_corrcoef,accuracy_score,roc_auc_score,precision_score,recall_score,f1_score,confusion_matrix

import math
import gzip
import pickle

import horovod.tensorflow.keras as hvd



# Load data
all_data,all_labels,all_seq = pickle.load( gzip.open( "processed_proteins_with_subtypes_HA_Human_vs_Avian_vs_Swine_multi.plkz", "rb" ) )

all_data = np.array(all_data)
all_labels = np.array(all_labels)

all_labels = to_categorical(all_labels, num_classes=3)


# Horovod: initialize Horovod.
hvd.init()


# Pin GPU to be used to process local rank (one GPU per process)
gpus = tf.config.experimental.list_physical_devices('GPU')

# print(gpus)
# input()

for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)
if gpus:
    tf.config.experimental.set_visible_devices(gpus[hvd.local_rank()], 'GPU')


# Batch Dataset
BATCH_SIZE = 300
EPOCHS = 1000

# Horovod: adjust number of epochs based on number of GPUs.
EPOCHS = int(math.ceil(EPOCHS / hvd.size()))


# Horovod: adjust learning rate based on number of GPUs.
opt = tf.optimizers.Adam(0.001 * hvd.size())

# Horovod: add Horovod Distributed Optimizer.
opt = hvd.DistributedOptimizer(opt)



# Write results
file_results = open('training_results_best.txt', 'w')
file_results.write('train_acc,val_acc,test_val,train_mcc,val_mcc,test_mcc,train_f1,val_f1,test_f1')
file_results.write('\n')
file_results.close()


file_results_class = open('training_results_best_class.txt', 'w')
file_results_class.write('test_acc_human,test_acc_avian,test_acc_swine')
file_results_class.write('\n')
file_results_class.close()


for i in range(100):


	r = range(len(all_data))
	r_train, r_test = train_test_split(r, test_size=0.10, stratify=all_labels)

	tX_train = all_data[r_train]
	tY_train = all_labels[r_train]
	X_test = all_data[r_test]
	Y_test = all_labels[r_test]


	r = range(len(tX_train))
	r_train, r_test = train_test_split(r, test_size=0.11,stratify=tY_train)

	X_train = tX_train[r_train]
	Y_train = tY_train[r_train]
	X_val = tX_train[r_test]
	Y_val = tY_train[r_test]


	model = Sequential()
	model.add(Conv1D(32, 3, input_shape=(576,1), activation=None, padding='same', strides=1, name='conv1',use_bias=True))
	model.add(LeakyReLU())
	model.add(MaxPool1D(2,padding="same"))

	model.add(Conv1D(32, 3, activation=None, padding='same', strides=1, name='conv2',use_bias=True))
	model.add(LeakyReLU())
	model.add(MaxPool1D(2,padding="same"))

	model.add(Conv1D(32, 3, activation=None, padding='same', strides=1, name='conv3',use_bias=True))
	model.add(LeakyReLU())
	model.add(MaxPool1D(2,padding="same"))


	model.add(Flatten())
	model.add(Dense(128, activation=None, name='fc1'))
	model.add(LeakyReLU())
	model.add(Dense(64, activation=None, name='fc2'))
	model.add(LeakyReLU())
	model.add(Dense(3, activation='softmax', name='output'))

	
	model.compile(optimizer=opt,
		loss=tf.losses.categorical_crossentropy,
		metrics=['accuracy'],
		experimental_run_tf_function=False)


	callbacks = [
	    # Horovod: broadcast initial variable states from rank 0 to all other processes.
	    # This is necessary to ensure consistent initialization of all workers when
	    # training is started with random weights or restored from a checkpoint.
	    hvd.callbacks.BroadcastGlobalVariablesCallback(0),
	]

	# Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
	if hvd.rank() == 0:
	    callbacks.append(ModelCheckpoint('weights/trained_model_weights_v'+str(int(i))+'_best_val.h5', monitor='val_loss', verbose=1, save_best_only=True, mode='min',save_weights_only=True))

	
	# Train Model
	model.fit(X_train,Y_train, 
		validation_data=(X_val,Y_val), 
		# shuffle=True, 
		epochs=EPOCHS, 
		# batch_size=BATCH_SIZE,
		steps_per_epoch=BATCH_SIZE // hvd.size(), 
		callbacks=callbacks,
		verbose=1 if hvd.rank() == 0 else 0)



	if hvd.rank() == 0:
		# Load Model
		model.load_weights('weights/trained_model_weights_v'+str(int(i))+'_best_val.h5')
		
		
		predicted_train = model.predict(X_train,batch_size=BATCH_SIZE,)
		predicted_val = model.predict(X_val,batch_size=BATCH_SIZE,)
		predicted_test = model.predict(X_test,batch_size=BATCH_SIZE,)

		predicted_train = [int(np.round(np.argmax(x))) for x in predicted_train]
		predicted_val = [int(np.round(np.argmax(x))) for x in predicted_val]
		predicted_test = [int(np.round(np.argmax(x))) for x in predicted_test]

		y_train = [int(np.round(np.argmax(x))) for x in Y_train]
		y_val = [int(np.round(np.argmax(x))) for x in Y_val]
		y_test  = [int(np.round(np.argmax(x))) for x in Y_test]


		train_acc = accuracy_score(np.array(y_train),np.array(predicted_train))
		val_acc = accuracy_score(np.array(y_val),np.array(predicted_val))
		test_acc =accuracy_score(np.array(y_test),np.array(predicted_test))

		matrix = confusion_matrix(np.array(y_test),np.array(predicted_test))
		test_acc_human,test_acc_avian,test_acc_swine = matrix.diagonal()/matrix.sum(axis=1)

		mcc_train = matthews_corrcoef(np.array(y_train),np.array(predicted_train))
		mcc_val = matthews_corrcoef(np.array(y_val),np.array(predicted_val))
		mcc_test = matthews_corrcoef(np.array(y_test),np.array(predicted_test))

		f1_train = f1_score(np.array(y_train),np.array(predicted_train),average='micro')
		f1_val = f1_score(np.array(y_val),np.array(predicted_val),average='micro')
		f1_test = f1_score(np.array(y_test),np.array(predicted_test),average='micro')


		file_results_class = open('training_results_best_class.txt', 'a')
		file_results_class.write(str(test_acc_human))
		file_results_class.write(',')
		file_results_class.write(str(test_acc_avian))
		file_results_class.write(',')
		file_results_class.write(str(test_acc_swine))
		file_results_class.write(',')
		file_results_class.write('\n')
		file_results_class.close()


		file_results = open('training_results_best.txt', 'a')
		file_results.write(str(train_acc))
		file_results.write(',')
		file_results.write(str(val_acc))
		file_results.write(',')
		file_results.write(str(test_acc))
		file_results.write(',')
		
		file_results.write(str(mcc_train))
		file_results.write(',')
		file_results.write(str(mcc_val))
		file_results.write(',')
		file_results.write(str(mcc_test))
		file_results.write(',')
		
		file_results.write(str(f1_train))
		file_results.write(',')
		file_results.write(str(f1_val))
		file_results.write(',')
		file_results.write(str(f1_test))


		file_results.write('\n')
		file_results.close()


	del model

